import requests
import secrets
import json

bitcoin_api_url = 'https://pro-api.coinmarketcap.com//v1/cryptocurrency/listings/latest'
key = secrets.api_key
def get_latest_bitcoin_price():
    custom_headers = {'X-CMC_PRO_API_KEY' : key }
    response = requests.get(bitcoin_api_url,headers = custom_headers)
    response_json = response.json()
    if response_json ['status']['error_code'] == 0:
        print(json.dumps(response_json["data"][0], sort_keys=True, indent=4))
    else:
        print (response_json['status'])
         
get_latest_bitcoin_price()